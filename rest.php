<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// Connessione al db
$host = "127.0.0.1";
$db_name = "students";
$username = "root";
$password = "";
try{
	$conn = new PDO("mysql:host=" .$host. ";dbname=" .$db_name, $username, $password);
}catch(PDOException $exception){
	echo "Errore di connessione: " . $exception->getMessage();
}

// Oggetto Student
include_once 'student.php';
$student = new Student($conn);

// Se la richiesta è GET restituisco gli studenti in formato JSON
if ($_SERVER['REQUEST_METHOD'] === 'GET'){
	
	// Conto gli studenti
	$queryn = "SELECT id FROM student";
	$resn = $conn->prepare($queryn);
	$resn->execute();
	$student_arr["Studenti"]=array();
	while ($rown = $resn->fetch(PDO::FETCH_ASSOC)){
		// Estraggo gli studenti e li mando in formato JSON
		$student = new Student($conn);
		$student->read($rown['id']);
		$student_json=array(
			"id" => $student->id,
			"fistname" => $student->firstname,
			"lastname" => $student->lastname,
			"birthdate" => $student->birthdate,
			"grades" => $student->grades,
			"avg" => $student->avg_grade(),
			"age" => $student->age()
		);
		array_push($student_arr["Studenti"], $student_json);
	}
	http_response_code(200);
	echo json_encode($student_arr["Studenti"]);

}

// Se la richiesta è POST inserisco uno studente in formato JSON
if ($_SERVER['REQUEST_METHOD'] === 'POST'){
	$data = json_decode(file_get_contents("php://input"));
	if(
		!empty($data->firstname) &&
		!empty($data->lastname) &&
		!empty($data->birthdate) 
		){
		// Prendo i dati da inserire
		$student->firstname = $data->firstname;
		$student->lastname = $data->lastname;
 		$student->birthdate = $data->birthdate;
		$student->grades = $data->grades;
		
		// Inserisco lo studente
		if($student->insert()){
			http_response_code(201);
			echo json_encode(array("messaggio" => "Studente inserito"));
		}else{
			http_response_code(503);
			echo json_encode(array("messaggio" => "Studente non inserito"));
		}
	}else{
		http_response_code(400);
		echo json_encode(array("messaggio" => "Manca qualche dato"));
	}
}
?>