<?php

function fibonacci($i)
{
	//https://en.wikipedia.org/wiki/Fibonacci_number#Computation_by_rounding
	return round(((5 ** 0.5 + 1) / 2) ** $i / 5 ** 0.5);
}

echo "<!DOCTYPE html>
	<html>
		<head>
		  <title>Fase 1</title>
		  <link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\">

		  <style>
			body {
			  font-family: 'Roboto', sans-serif;
			  font-size: 36px;
			  margin-top: 20px;
			  text-align: center;
			  background-color: #91A8d0;
			  color: white;

			}
		  </style>
		</head>
		<body>
	";
	
$n = 4; // Numero di cifre da cercare
$i = 0; // Indice
$fib = 1; //Numero di Fibonacci calcolato

while (log10($fib)+1 < $n){
	$fib = fibonacci($i++);
}

echo "Il primo numero a ".$n." cifre è F(".($i-1).") = ".$fib."
		</body>
	</html>
	";
	
?>