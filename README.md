# Soluzioni all' Alpenite Practice Test
# Golfetto Thomas

Soluzione realizzata con **PHP/7.3.2**.
Per il test sono stati utilizzati:

- XAMMP/3.2.3 con Apache/2.4.38 e MySQL Ver 15.1 Distrib 10.1.38-MariaDB
- Postman/7.0.6.

## Fase 1
Risolvi il seguente problema con uno script nel linguaggio di programmazione che preferisci.

```
I primi dodici numeri della sequenza di Fibonacci sono:
1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144

Il 12° numero è il primo della sequenza a essere di 3 cifre. Quale è il primo numero della sequenza ad avere 1000 cifre?
```

## Soluzione 1

Nel file `fase1.php` è presente la funzione che ritorna il primo numero di Fibonacci che ha `$n` cifre, dove `$n` in questo caso è 4. 
Il numero di cifre di un numero intero `$fib` è
```
log10($fib)+1
```
E uno dei modi più veloci per il calcolo di un numero di Fibonacci è tramite la formula di Binet

## Fase 2

Descrivi nel linguaggio di programmazione che preferisci un oggetto:

- `Student` con le seguenti proprietà: `firstname` : string, `lastname` : string, `birthdate` : string, `grades` : string

Crea dei metodi d'appoggio sul modello `Student` per calcolare `age` e `avg_grade`

## Soluzione 2

Oltre alle variabili e al costruttore, la classe `Student` (nel file `student.php`) presenta i metodi:

- `read($i)` per l'estazione da database dell' i-esimo studente
- `insert()` per l'intserimento nel database di un nuovo studente coi relativi voti
- `avg_grade()` che calcola la media dei voti del relativo studente
- `age()` che fornisce la differenza in anni tra la data odierna e la data di nascita dello studente

Nel database i voti e gli studenti sono salvati in diverse tabelle per una maggiore scalabilità del progetto (in caso ad esempio si voglia aggiungere il nome dell'esame a cui fa riferimento il voto, ecc).
Le tabelle in questione sono nei file `student.sql` e `grade.sql`.

## Fase 3

Crea nel linguaggio di programmazione che preferisci un webservice REST (può girare in locale) in grado di ricevere richieste:

- `POST` per la creazione di un nuovo `Student`
- `GET` per il retrieve di tutti gli `Student` creati

Gli studenti che vengono creati possono essere salvati in un array/list di appoggio che vive solo per la durata dell'esecuzione del server.

Non è necessario creare un'interfaccia per fare le chiamate `POST` e `GET`, è sufficiente un esempio di utilizzo in [Postman](https://www.getpostman.com) o [cURL](https://curl.haxx.se/docs/manpage.html)

## Soluzione 3

Il file `rest.php` accetta chiamate `POST` e `GET` e risponde come richiesto:

- In caso di chiamata `GET` estrae gli studenti dalla relativa tabella, e per ognuno di essi ne concatena la lista dei voti, estratta dalla tabella `grade`, in una stringa intervallata da virgole, salvata poi nella variabile dell'oggetto `Student`. La lista degli studenti con le relative informazioni calcolate è poi restituita in formato JSON. 
- In caso di chiamata `POST` con dati inviati in formato JSON, previ controlli basilari sui dati, viene inserito lo studente in tabella coi relativi voti. In caso non ci siano voti lo studente viene inserito comunque.

I dati inviati sono nel formato:
```
{
    "firstname" : "Gianni",
    "lastname" : "SenzaVoti",
    "birthdate" : "2004-12-02",
    "grades" : "3,5,6"
}
```
I dati ottenuti sono nel formato:
```
{
[
    {
        "id": "1",
        "fistname": "Thomas",
        "lastname": "Golfetto",
        "birthdate": "1994-07-12",
        "grades": "10,7,4,10",
        "avg": 7.75,
        "age": 24
    },
    {
        "id": "2",
        "fistname": "Pinco",
        "lastname": "Pallino",
        "birthdate": "1998-03-27",
        "grades": "6",
        "avg": 6,
        "age": 20
    },
    {
        "id": "7",
        "fistname": "Mario",
        "lastname": "Verdi",
        "birthdate": "2002-07-02",
        "grades": "5,4",
        "avg": 4.5,
        "age": 16
    },
    {
        "id": "8",
        "fistname": "Gianni",
        "lastname": "SenzaVoti",
        "birthdate": "2004-12-02",
        "grades": null,
        "avg": 0,
        "age": 14
    }
]
}
```