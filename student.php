<?php
class Student{
	
    // Connessione al database
    private $conn;
 
	// Variabili
    public $id;
    public $firstname;
    public $lastname;
    public $birthdate;
	public $grades;

    // Nel costruttore prendo il riferimento della connessione al db
    public function __construct($db){
        $this->conn = $db;
    }
	
	// Estrai i dati dal db
	function read($i){
		$query = "SELECT student.id,student.firstname,student.lastname,student.birthdate, 
			GROUP_CONCAT(grade.value) AS grades FROM student LEFT JOIN grade ON student.id = grade.student WHERE student.id = '".$i."' GROUP by student.id";
		$res = $this->conn->prepare($query);
		$res->execute();
		$row = $res->fetch(PDO::FETCH_ASSOC);
		$this->id = $row['id'];
		$this->firstname = $row['firstname'];
		$this->lastname = $row['lastname'];
		$this->birthdate = $row['birthdate'];
		$this->grades = $row['grades'];
		return $res;
	}
	
	// Inserisco i dati nel db
	function insert(){
		
		$this->firstname=htmlspecialchars(strip_tags($this->firstname));
		$this->lastname=htmlspecialchars(strip_tags($this->lastname));
		list($year,$month,$day) = explode("-",$this->birthdate);
		if (!checkdate ($month,$day,$year)) return false;
		
		$query = "INSERT INTO `student`(`firstname`, `lastname`, `birthdate`) VALUES ('".$this->firstname."','".$this->lastname."','".$this->birthdate."')";
		$res = $this->conn->prepare($query);
		$res->execute();
		
		$grades_arr = explode(",",$this->grades);
		$stud_id = $this->conn->lastInsertId();
		for ($i = 0; $i < count($grades_arr); $i++){
			$query = "INSERT INTO `grade`(`student`, `value`) VALUES (".$stud_id.",".$grades_arr[$i].")";
			$res = $this->conn->prepare($query);
			$res->execute();
		}
		
		return true;
	}
	
	function avg_grade(){
		if(!is_null($this->grades)){
			$grades_arr = explode(",",$this->grades);
			return array_sum($grades_arr)/count($grades_arr);
		}else{
			return 0;
		}
	}
	
	function age(){
		if(!is_null($this->birthdate)){
			$from = new DateTime($this->birthdate);
			$to = new DateTime('today');
			return $from->diff($to)->y;
		}else{
			return 0;
		}	
	}
}
?>